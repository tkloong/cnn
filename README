# Description

This is the Tensorflow experiment code of CNN, trained with stochastic gradient.

# How to prepare data?

For data in LIBSVM format (https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/), please use 'cnn_data_engine.py' to process data.

# How to run?

1. First, use *libsvm_pickle.py* to convert LIBSVM data into pickle format. Move *train_data.pkl* and *test_data.pkl* to *./data/$dataset_name/*. For example,
```
python3 libsvm_pickle.py -f 784 -train /home/data/mnist.scale -test /home/data/mnist.scale.t
mkdir -p ./data/mnist
mv train_data.pkl test_data.pkl ./data/mnist/
```

2. Run *cnn.py*. For cifar10:
    ```
    python3 cnn.py -lr 1e-2 -dn cifar10 -ht 32 -wt 32 -c 3 -train /home/data/cifar10_RGB_255.scale -test /home/data/cifar10_RGB_255.scale.t
    ```

For mnist:
    ```
    python3 cnn.py -lr 1e-2 -dn mnist -ht 28 -wt 28 -c 1 -train /home/data/mnist.scale -test /home/data/mnist.scale.t
    ```

# Arguments list

Please see the manual.
    ```
    python3 cnn.py -h
    ```
