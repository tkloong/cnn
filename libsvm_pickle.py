import sys
import argparse
import numpy
import time
try:
    import cPickle as pickle
except:
    import pickle
from cnn_data_engine import svm_read_problem

def ArgsParser():
  class MyParser(argparse.ArgumentParser):
    def error(self, message):
      sys.stderr.write('error: %s\n' % message)
      self.print_help()
      sys.exit(2)

  parser = MyParser()
  parser.add_argument('-f', '--num_features', help='Number of features', required=True, type=int)
  parser.add_argument('-train', '--train_path', help='specify train path', default='', type=str)
  parser.add_argument('-test', '--test_path', help='specify test path', default='', type=str)
  args = parser.parse_args()
  return args

if __name__ == '__main__':
    """
    If all arguments are set, this program will output two file: `train_data.pkl` and `test_data.pkl`.
    Each file contains a tuple of numpy array, (y, x).
    y is a label vector, stored as one dimensional numpy array, E.g. [ 7.  2.  1. ...,  4.  5.  6.]
    x is a data matrix, stored as two dimensional numpy array, x[i][j] represent
    the j-th feature of i-th instance.
    """
    args = ArgsParser()
    train_path = args.train_path
    test_path = args.test_path
    num_features = args.num_features

    print('Convert train data')
    try:
        # Read libsvm data
        s = time.time()
        y_train, x_train = svm_read_problem(train_path, num_features)
        e = time.time() - s
        print('Time: %g' % e)
        x_train = numpy.array(x_train)
        y_train = numpy.array(y_train)

        # Save data using pickle
        filehandler = open('train_data.pkl', 'wb')
        pickle.dump((y_train, x_train), filehandler)
        filehandler.close()
    except Exception as e:
        print(str(e))

    print('Convert test data')
    try:
        s = time.time()
        y_test, x_test = svm_read_problem(test_path, num_features)
        e = time.time() - s
        print('Time: %g' % e)
        x_test = numpy.array(x_test)
        y_test = numpy.array(y_test)
        print(y_test)

        # Save data using pickle
        filehandler = open('test_data.pkl', 'wb')
        pickle.dump((y_test, x_test), filehandler)
        filehandler.close()
    except Exception as e:
        print(str(e))
